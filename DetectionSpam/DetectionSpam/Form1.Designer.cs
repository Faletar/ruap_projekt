﻿namespace DetectionSpam
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Submit = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.Ponovi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RezultatSH = new System.Windows.Forms.Label();
            this.TocnostSH = new System.Windows.Forms.Label();
            this.WriteText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(550, 349);
            this.Submit.Margin = new System.Windows.Forms.Padding(4);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(114, 42);
            this.Submit.TabIndex = 0;
            this.Submit.Text = "Provjeri";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(15, 34);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(649, 307);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // Ponovi
            // 
            this.Ponovi.Location = new System.Drawing.Point(424, 349);
            this.Ponovi.Name = "Ponovi";
            this.Ponovi.Size = new System.Drawing.Size(114, 42);
            this.Ponovi.TabIndex = 2;
            this.Ponovi.Text = "Resetiraj";
            this.Ponovi.UseVisualStyleBackColor = true;
            this.Ponovi.Click += new System.EventHandler(this.Ponovi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 349);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Klasa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 376);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Točnost:";
            // 
            // RezultatSH
            // 
            this.RezultatSH.AutoSize = true;
            this.RezultatSH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RezultatSH.Location = new System.Drawing.Point(80, 349);
            this.RezultatSH.Name = "RezultatSH";
            this.RezultatSH.Size = new System.Drawing.Size(0, 17);
            this.RezultatSH.TabIndex = 5;
            // 
            // TocnostSH
            // 
            this.TocnostSH.AutoSize = true;
            this.TocnostSH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TocnostSH.Location = new System.Drawing.Point(80, 376);
            this.TocnostSH.Name = "TocnostSH";
            this.TocnostSH.Size = new System.Drawing.Size(0, 17);
            this.TocnostSH.TabIndex = 6;
            // 
            // WriteText
            // 
            this.WriteText.AutoSize = true;
            this.WriteText.Location = new System.Drawing.Point(12, 13);
            this.WriteText.Name = "WriteText";
            this.WriteText.Size = new System.Drawing.Size(108, 17);
            this.WriteText.TabIndex = 7;
            this.WriteText.Text = "Unesite poruku:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 406);
            this.Controls.Add(this.WriteText);
            this.Controls.Add(this.TocnostSH);
            this.Controls.Add(this.RezultatSH);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Ponovi);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.Submit);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Spam Detekcija";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button Ponovi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label RezultatSH;
        private System.Windows.Forms.Label TocnostSH;
        private System.Windows.Forms.Label WriteText;
    }
}

