﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DetectionSpam
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private async Task InvokeRequestResponseService()
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"classification", "message"},
                                Values = new string[,] {  { "text", richTextBox1.Text } }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };
                const string apiKey = "Ifs3QxVd1YEXMQXonu2YuIg64NcTVNlSwFVgNU3HTTwo45uRw5YqfnV2EWSd8E71ZTbSXJQzK3SSakfhiirucg=="; // Replace this with the API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/7d7609295e3c445fae42ffea3c471575/services/87d06eaa52584ce0b11d0b807cdd1348/execute?api-version=2.0&details=true");

                // WARNING: The 'await' statement below can result in a deadlock if you are calling this code from the UI thread of an ASP.Net application.
                // One way to address this would be to call ConfigureAwait(false) so that the execution does not attempt to resume on the original context.
                // For instance, replace code such as:
                //      result = await DoSomeTask()
                // with the following:
                //      result = await DoSomeTask().ConfigureAwait(false)


                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode)
                {
                    double percent;
                    string resClass = "", resPercentage = "";
                    string result = await response.Content.ReadAsStringAsync();                  

                    resClass = GiveMeParsedResult(result, 4);
                    resPercentage = GiveMeParsedResult(result, 2);

                    percent = float.Parse(resPercentage, CultureInfo.InvariantCulture.NumberFormat);
                    percent *= 100;
                    if (resClass == "Ham") percent = 100 - percent;
                    RezultatSH.Text = resClass;
                    TocnostSH.Text = percent.ToString("0.00") + " %";
                }
                else
                {
                    Console.WriteLine(string.Format("The request failed with status code: {0}", response.StatusCode));

                    // Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
                    Console.WriteLine(response.Headers.ToString());

                    string responseContent = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseContent);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InvokeRequestResponseService();
        }

        private string GiveMeParsedResult(string result, int num)
        {
            string myResult = "";
            int countNav = 0, position = 0;
            for (int i = result.Length - 1; countNav != num; i--)
            {
                if (result[i] == '"')
                {
                    countNav++;
                    if (countNav == num)
                    {
                        position = i;
                    }
                }
            }
            for (int i = position + 1; i < result.Length; i++)
            {
                if (result[i] == '"') break;
                myResult += result[i].ToString();
            }
            if (myResult == "ham") myResult = "Ham";
            else if (myResult == "spam") myResult = "Spam";

            return myResult;
        }
                     
        private void Ponovi_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            RezultatSH.Text = "";
            TocnostSH.Text = "";
        }
               
    }

    public class StringTable
    {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }
}
